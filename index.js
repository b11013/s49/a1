//use fetch to get data from the server
//it'll request from server then load the request (API)
//will return a format of JSON

//syntax:
//fetch('url', options)
//url - request url
//options - array of properties, optional parameters
//default behavior is retrieval so options is optional

//*show posts - .then to show result

fetch("https://jsonplaceholder.typicode.com/posts")
    .then((res) => res.json())
    .then((data) => showPosts(data));
//data is the result of the request

const showPosts = (posts) => {
    let postEntries = ""; //we assign the value of the postEntries variable to an empty string
    posts.forEach((post) => {
        console.log(post);
        postEntries += `
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick="editPost('${post.id}')">Edit</button>
            <button onclick="deletePost('${post.id}')">Delete</button>
        </div>
        `;
    });
    document.querySelector("#div-post-entries").innerHTML = postEntries;
};

//add post entries
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
    e.preventDefault();
    //options is used when using diff method
    fetch("https://jsonplaceholder.typicode.com/posts", {
            method: "POST",
            body: JSON.stringify({
                title: document.querySelector("#txt-title").value,
                body: document.querySelector("#txt-body").value,
                userId: 1,
            }),
            headers: {
                "Content-Type": "application/json",
            },
        })
        .then((res) => res.json())
        .then((data) => {
            console.log(data);
            alert("Successfully added.");
            document.querySelector("#txt-title").value = null;
            document.querySelector("#txt-body").value = null;
        });
});

//editPost
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;

    document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

//update posts
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();
    fetch(`https://jsonplaceholder.typicode.com/posts/2`, {
            method: "PUT",
            body: JSON.stringify({
                id: document.querySelector("#txt-edit-id").value,
                title: document.querySelector("#txt-edit-title").value,
                body: document.querySelector("#txt-edit-body").value,
                userId: 1,
            }),
            headers: {
                "Content-Type": "application/json",
            },
        })
        .then((res) => res.json())
        .then((data) => {
            console.log(data);
            alert("Post is successfully updated.");
            document.querySelector("#txt-edit-id").value = null;
            document.querySelector("#txt-edit-title").value = null;
            document.querySelector("#txt-edit-body").value = null;
            document
                .querySelector("#btn-submit-update")
                .setAttribute("disabled", true);
        });
});

//activity: delete post
//deletePost
const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        })
        .then((res) => res.json())
        .then((data) => {
            document.querySelector(`#post-${id}`).remove();
            alert("Post is successfully deleted.");
        });
};